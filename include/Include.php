<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/permission/model/repository/PermissionRepository.php');

include($strRootPath . '/src/permission/specification/library/ConstPermSpec.php');
include($strRootPath . '/src/permission/specification/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/permission/specification/exception/KeyNotFoundException.php');
include($strRootPath . '/src/permission/specification/model/DefaultPermSpec.php');

include($strRootPath . '/src/permission/subject/library/ConstSubjPerm.php');
include($strRootPath . '/src/permission/subject/model/SubjPermEntity.php');
include($strRootPath . '/src/permission/subject/model/SubjPermEntityCollection.php');
include($strRootPath . '/src/permission/subject/model/SubjPermEntityFactory.php');

include($strRootPath . '/src/role/requisition/request/info/library/ConstRoleSndInfo.php');

include($strRootPath . '/src/role/permission/library/ConstRolePerm.php');
include($strRootPath . '/src/role/permission/model/RolePermEntity.php');
include($strRootPath . '/src/role/permission/model/RolePermEntityCollection.php');
include($strRootPath . '/src/role/permission/model/RolePermEntityFactory.php');

include($strRootPath . '/src/role/library/ConstRole.php');
include($strRootPath . '/src/role/exception/RolePermEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/role/exception/RolePermEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/role/model/RoleEntity.php');
include($strRootPath . '/src/role/model/RoleEntityCollection.php');
include($strRootPath . '/src/role/model/RoleEntityFactory.php');
include($strRootPath . '/src/role/model/repository/RoleEntityMultiRepository.php');
include($strRootPath . '/src/role/model/repository/RoleEntityMultiCollectionRepository.php');

include($strRootPath . '/src/role/browser/library/ConstRoleBrowser.php');
include($strRootPath . '/src/role/browser/model/RoleBrowserEntity.php');

include($strRootPath . '/src/profile/library/ConstRoleProfile.php');
include($strRootPath . '/src/profile/exception/SubjPermEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/profile/exception/RoleEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/profile/model/RoleProfileEntity.php');
include($strRootPath . '/src/profile/model/RoleProfileEntityFactory.php');

include($strRootPath . '/src/profile/browser/library/ConstRoleProfileBrowser.php');
include($strRootPath . '/src/profile/browser/model/RoleProfileBrowserEntity.php');

include($strRootPath . '/src/requisition/request/info/factory/library/ConstRoleConfigSndInfoFactory.php');
include($strRootPath . '/src/requisition/request/info/factory/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/requisition/request/info/factory/model/RoleConfigSndInfoFactory.php');