<?php
/**
 * This class allows to define subject permission entity collection class.
 * Subject permission entity collection is basic subject permission entity collection,
 * used to store subject permission entities.
 * key => SubjPermEntity.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\role\permission\subject\model;

use liberty_code\role_model\permission\subject\model\SubjPermEntityCollection as BaseSubjPermEntityCollection;

use people_sdk\role\permission\subject\model\SubjPermEntity;



/**
 * @method null|SubjPermEntity getItem(string $strKey) @inheritdoc
 * @method null|SubjPermEntity getObjPermission(string $strKey) @inheritdoc
 * @method string setItem(SubjPermEntity $objEntity) @inheritdoc
 * @method string setPermission(SubjPermEntity $objPermission) @inheritdoc
 * @method SubjPermEntity removePermission(string $strKey) @inheritdoc
 */
class SubjPermEntityCollection extends BaseSubjPermEntityCollection
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixItemClassPath()
    {
        // Return result
        return SubjPermEntity::class;
    }



}