<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\role\permission\specification\library;



class ConstPermSpec
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_REPO_EXECUTION_CONFIG = 'repo_execution_config';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default permission specification configuration standard.';
    const EXCEPT_MSG_KEY_NOT_FOUND = 'Permission keys not found!';



}