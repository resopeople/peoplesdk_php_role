<?php
/**
 * Description :
 * This class allows to define default permission specification class.
 * Default permission specification uses permission repository,
 * to provide a scope of permission keys, for a space.
 *
 * Default permission specification uses the following specified configuration:
 * [
 *     Default permission specification configuration,
 *
 *     repo_execution_config(optional): [
 *         @see PermissionRepository::getTabPermissionKey() configuration array format
 *     ]
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\role\permission\specification\model;

use liberty_code\role\permission\specification\model\DefaultPermSpec as BaseDefaultPermSpec;

use liberty_code\cache\repository\api\RepositoryInterface;
use liberty_code\role\permission\specification\library\ConstPermSpec as BaseConstPermSpec;
use people_sdk\role\permission\model\repository\PermissionRepository;
use people_sdk\role\permission\specification\library\ConstPermSpec;
use people_sdk\role\permission\specification\exception\ConfigInvalidFormatException;
use people_sdk\role\permission\specification\exception\KeyNotFoundException;



class DefaultPermSpec extends BaseDefaultPermSpec
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * DI: Permission repository instance.
     * @var PermissionRepository
     */
    protected $objPermissionRepo;



	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param PermissionRepository $objPermissionRepo
     * @param null|array $tabMacroPermRepoExecConfig = null
     */
    public function __construct(
        PermissionRepository $objPermissionRepo,
        array $tabConfig = null,
        RepositoryInterface $objCacheRepo = null
    )
    {
        // Init properties
        $this->objPermissionRepo = $objPermissionRepo;

        // Call parent constructor
        parent::__construct(
            $tabConfig,
            $objCacheRepo
        );
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Validation
        try
        {
            switch($key)
            {
                case BaseConstPermSpec::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get repository execution configuration array.
     *
     * @return null|array
     */
    public function getTabRepoExecConfig()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstPermSpec::TAB_CONFIG_KEY_REPO_EXECUTION_CONFIG]) ?
                $tabConfig[ConstPermSpec::TAB_CONFIG_KEY_REPO_EXECUTION_CONFIG] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws KeyNotFoundException
     */
    protected function getTabKeyEngine()
    {
        // Init var
        $result = $this
            ->objPermissionRepo
            ->getTabPermissionKey($this->getTabRepoExecConfig());

        // Set check argument
        if(is_null($result))
        {
            throw new KeyNotFoundException();
        }

        // Return result
        return $result;
    }



}