<?php
/**
 * This class allows to define permission repository class.
 * Permission repository allows to retrieve permissions information,
 * using HTTP request sending and HTTP response reception, via requisition requester.
 * Specified requisition requester must be able to use HTTP client, HTTP request and data HTTP response, with json parsing,
 * to handle HTTP request sending and HTTP response reception.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\role\permission\model\repository;

use liberty_code\library\bean\model\FixBean;

use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\parser\parser\factory\library\ConstParserFactory;
use liberty_code\parser\parser\factory\string_table\library\ConstStrTableParserFactory;
use liberty_code\requisition\request\factory\library\ConstRequestFactory;
use liberty_code\requisition\response\factory\library\ConstResponseFactory;
use liberty_code\requisition\client\info\library\ConstInfoClient;
use liberty_code\requisition\requester\api\RequesterInterface;
use liberty_code\requisition\requester\client\library\ConstClientRequester;
use liberty_code\http\requisition\request\library\ConstHttpRequest;
use liberty_code\http\requisition\request\factory\library\ConstHttpRequestFactory;
use liberty_code\http\requisition\response\data\library\ConstDataHttpResponse;
use liberty_code\http\requisition\response\data\model\DataHttpResponse;
use liberty_code\http\requisition\response\factory\library\ConstHttpResponseFactory;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\library\requisition\response\library\ToolBoxResponse;



class PermissionRepository extends FixBean
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * DI: Requester instance.
     * @var RequesterInterface
     */
    protected $objRequester;



    /**
     * DI: Request sending information factory instance.
     * @var SndInfoFactoryInterface
     */
    protected $objRequestSndInfoFactory;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param RequesterInterface $objRequester
     * @param SndInfoFactoryInterface $objRequestSndInfoFactory
     */
    public function __construct(
        RequesterInterface $objRequester,
        SndInfoFactoryInterface $objRequestSndInfoFactory
    )
    {
        // Init properties
        $this->objRequester = $objRequester;
        $this->objRequestSndInfoFactory = $objRequestSndInfoFactory;

        // Call parent constructor
        parent::__construct();
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get index array of permission keys.
     * Response can be returned, if required.
     *
     * Configuration array format:
     * Execution configuration can be provided:
     * @see RequesterInterface::executeRequestConfig() configuration array format.
     *
     * @param null|array $tabConfig = null
     * @param null|DataHttpResponse &$objResponse = null
     * @return null|array
     */
    public function getTabPermissionKey(
        array $tabConfig = null,
        DataHttpResponse &$objResponse = null
    )
    {
        // Init var
        $tabExecConfig = $tabConfig;
        $tabConfig = array(
            ConstClientRequester::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG => [
                ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG => [
                    ConstResponseFactory::TAB_CONFIG_KEY_TYPE => ConstHttpResponseFactory::CONFIG_TYPE_HTTP_DATA,
                    ConstDataHttpResponse::TAB_CONFIG_KEY_BODY_PARSER_CONFIG => [
                        ConstParserFactory::TAB_CONFIG_KEY_TYPE => ConstStrTableParserFactory::CONFIG_TYPE_STR_TABLE_JSON
                    ]
                ]
            ]
        );
        $tabConfig = (
            (!is_null($tabExecConfig)) ?
                ToolBoxTable::getTabMerge($tabExecConfig, $tabConfig):
                $tabConfig
        );
        /** @var null|DataHttpResponse $objResponse */
        $objResponse = $this
            ->objRequester
            ->executeRequestConfig(
                array(
                    ConstRequestFactory::TAB_CONFIG_KEY_TYPE => ConstHttpRequestFactory::CONFIG_TYPE_HTTP,
                    ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO => $this->objRequestSndInfoFactory->getTabSndInfo(
                        array(
                            ConstHttpRequest::TAB_SND_INFO_KEY_METHOD => 'GET',
                            ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE => '/permission'
                        )
                    )
                ),
                $tabConfig
            );
        $result = (
            (
                (!is_null($objResponse)) &&
                (!is_null($tabResultData = ToolBoxResponse::getTabResultData($objResponse))) &&
                is_array($tabResultData)
            ) ?
                array_filter(
                    array_values($tabResultData),
                    function($data) {return is_string($data);}
                ) :
                null
        );

        // Return result
        return $result;
    }



}


