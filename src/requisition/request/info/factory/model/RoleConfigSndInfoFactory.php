<?php
/**
 * Description :
 * This class allows to define role configuration sending information factory class.
 * Role configuration sending information factory uses role values, from source configuration object,
 * to provide HTTP request sending information.
 *
 * Role configuration sending information factory uses the following specified configuration:
 * [
 *     Default configuration sending information factory,
 *
 *     role_support_type(optional: got "header", if not found):
 *         "string support type, to set role options parameters.
 *         Scope of available values: @see ConstRoleConfigSndInfoFactory::getTabConfigSupportType()",
 *
 *     role_current_include_config_key(optional):
 *         "string configuration key, to get role current include option,
 *         used on role options",
 *
 *     role_perm_full_update_config_key(optional):
 *         "string configuration key, to get role permission full update option
 *         used on role options"
 * ]
 *
 * Role configuration sending information factory uses the following values,
 * from specified source configuration object, to get sending information:
 * {
 *     Value <role_current_include_config_key>(optional): true / false,
 *
 *     Value <role_perm_full_update_config_key>(optional): true / false
 * }
 *
 * Note:
 * -> Configuration support type:
 *     - "url_argument":
 *         Data put on URL arguments array.
 *     - "header":
 *         Data put on headers array.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\role\requisition\request\info\factory\model;

use people_sdk\library\requisition\request\info\factory\config\model\DefaultConfigSndInfoFactory;

use people_sdk\library\requisition\request\info\library\ToolBoxSndInfo;
use people_sdk\library\requisition\request\info\factory\library\ConstSndInfoFactory;
use people_sdk\role\role\requisition\request\info\library\ConstRoleSndInfo;
use people_sdk\role\requisition\request\info\factory\library\ConstRoleConfigSndInfoFactory;
use people_sdk\role\requisition\request\info\factory\exception\ConfigInvalidFormatException;



class RoleConfigSndInfoFactory extends DefaultConfigSndInfoFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstSndInfoFactory::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
	// ******************************************************************************

    /**
     * Get specified support type.
     *
     * @param string $strConfigKey
     * @param string $strDefault = ConstRoleConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER
     * @return string
     */
    protected function getStrSupportType(
        $strConfigKey,
        $strDefault = ConstRoleConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER
    )
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strDefault = (is_string($strDefault) ? $strDefault : ConstRoleConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER);
        $result = (
            isset($tabConfig[$strConfigKey]) ?
                $tabConfig[$strConfigKey] :
                $strDefault
        );

        // Return result
        return $result;
    }



    /**
     * Get new or specified sending information array.
     * with configured role current include option, used on role options.
     *
     * @param null|array $tabInfo = null
     * @return array
     */
    protected function getTabSndInfoWithRoleCurrentInclude(array $tabInfo = null)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strKey = (
            isset($tabConfig[ConstRoleConfigSndInfoFactory::TAB_CONFIG_KEY_ROLE_CURRENT_INCLUDE_CONFIG_KEY]) ?
                $tabConfig[ConstRoleConfigSndInfoFactory::TAB_CONFIG_KEY_ROLE_CURRENT_INCLUDE_CONFIG_KEY] :
                null
        );
        $result = $this->getTabSndInfoWithSrcConfigValue(
            $strKey,
            function($value){
                $boolOnHeaderRequired = (
                    $this->getStrSupportType(ConstRoleConfigSndInfoFactory::TAB_CONFIG_KEY_ROLE_SUPPORT_TYPE) ==
                    ConstRoleConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER
                );

                return ToolBoxSndInfo::getTabSndInfoWithParam(
                    ((intval($value) != 0) ? 1 : 0),
                    ($boolOnHeaderRequired ? ConstRoleSndInfo::HEADER_KEY_CURRENT_INCLUDE : null),
                    ((!$boolOnHeaderRequired) ? ConstRoleSndInfo::URL_ARG_KEY_CURRENT_INCLUDE : null)
                );
            },
            $tabInfo,
            function($value){
                return (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($value) ||
                    is_int($value) ||
                    (is_string($value) && ctype_digit($value))
                );
            }
        );

        // Return result
        return $result;
    }



    /**
     * Get new or specified sending information array.
     * with configured role permission full update option, used on role options.
     *
     * @param null|array $tabInfo = null
     * @return array
     */
    protected function getTabSndInfoWithRolePermFullUpdate(array $tabInfo = null)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strKey = (
            isset($tabConfig[ConstRoleConfigSndInfoFactory::TAB_CONFIG_KEY_ROLE_PERM_FULL_UPDATE_CONFIG_KEY]) ?
                $tabConfig[ConstRoleConfigSndInfoFactory::TAB_CONFIG_KEY_ROLE_PERM_FULL_UPDATE_CONFIG_KEY] :
                null
        );
        $result = $this->getTabSndInfoWithSrcConfigValue(
            $strKey,
            function($value){
                $boolOnHeaderRequired = (
                    $this->getStrSupportType(ConstRoleConfigSndInfoFactory::TAB_CONFIG_KEY_ROLE_SUPPORT_TYPE) ==
                    ConstRoleConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER
                );

                return ToolBoxSndInfo::getTabSndInfoWithParam(
                    ((intval($value) != 0) ? 1 : 0),
                    ($boolOnHeaderRequired ? ConstRoleSndInfo::HEADER_KEY_PERM_FULL_UPDATE : null),
                    ((!$boolOnHeaderRequired) ? ConstRoleSndInfo::URL_ARG_KEY_PERM_FULL_UPDATE : null)
                );
            },
            $tabInfo,
            function($value){
                return (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($value) ||
                    is_int($value) ||
                    (is_string($value) && ctype_digit($value))
                );
            }
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabSndInfoEngine()
    {
        // Init var
        $result = $this->getTabSndInfoWithRoleCurrentInclude();
        $result = $this->getTabSndInfoWithRolePermFullUpdate($result);

        // Return result
        return $result;
    }



}