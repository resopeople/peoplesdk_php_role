<?php
/**
 * This class allows to define role permission entity class.
 * Role permission entity is basic role permission entity,
 * related to a specific role.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\role\role\permission\model;

use liberty_code\role_model\role\permission\model\RolePermEntity as BaseRolePermEntity;

use DateTime;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\repository\library\ConstSaveEntity;
use liberty_code\role_model\permission\library\ConstPermission as BaseConstPermission;
use people_sdk\library\model\entity\library\ToolBoxEntity;
use people_sdk\role\role\permission\library\ConstRolePerm;



/**
 * @property integer $intAttrId @deprecated
 * @property string|DateTime $attrDtCreate @deprecated
 * @property string|DateTime $attrDtUpdate @deprecated
 * @property null|string $strAttrKey
 * @property null|boolean $boolAttrValue
 * @property integer $intAttrRoleId @deprecated
 */
class RolePermEntity extends BaseRolePermEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct(
        array $tabValue = array(),
        ValidatorInterface $objValidator = null
    )
    {
        // Call parent constructor
        parent::__construct($tabValue, $objValidator, null);
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrBaseKey()
    {
        // Init var
        $strKey = $this->getAttributeValue(BaseConstPermission::ATTRIBUTE_KEY_KEY);
        $result = (
            (is_string($strKey) && (trim($strKey) != '')) ?
                $strKey :
                static::getStrHash($this)
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Init var
        $tabUpdateConfig = array(
            // Select and update attribute key
            BaseConstPermission::ATTRIBUTE_KEY_KEY => [
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstRolePerm::ATTRIBUTE_NAME_SAVE_KEY,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Select and update attribute value
            BaseConstPermission::ATTRIBUTE_KEY_VALUE => [
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstRolePerm::ATTRIBUTE_NAME_SAVE_VALUE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ]
        );
        // Select attribute config
        $result = array_filter(
            parent::getTabConfig(),
            function(array $tabAttrConfig) use ($tabUpdateConfig) {
                $strAttrKey = $tabAttrConfig[ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY];

                return array_key_exists(
                    $strAttrKey,
                    $tabUpdateConfig
                );
            }
        );
        // Format attribute config
        $result = array_map(
            function(array $tabAttrConfig) use ($tabUpdateConfig) {
                $strAttrKey = $tabAttrConfig[ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY];
                $tabUpdateAttrConfig = $tabUpdateConfig[$strAttrKey];

                if(array_key_exists(ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE, $tabAttrConfig))
                {
                    unset($tabAttrConfig[ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE]);
                }

                return array_merge(
                    $tabAttrConfig,
                    $tabUpdateAttrConfig
                );
            },
            $result
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init var
        $tabRuleConfig = parent::getTabRuleConfig();
        $result = array_merge(
            $tabRuleConfig,
            array(
                BaseConstPermission::ATTRIBUTE_KEY_KEY => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => ['is_null'],
                                'is-valid-string' => $tabRuleConfig[BaseConstPermission::ATTRIBUTE_KEY_KEY]
                            ],
                            'error_message_pattern' => '%1$s must be null or a string not empty'
                        ]
                    ]
                ],
                BaseConstPermission::ATTRIBUTE_KEY_VALUE => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => ['is_null'],
                                'is-valid-boolean' => $tabRuleConfig[BaseConstPermission::ATTRIBUTE_KEY_VALUE]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null or a boolean.'
                    ]
                ]
            )
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case BaseConstPermission::ATTRIBUTE_KEY_KEY:
            case BaseConstPermission::ATTRIBUTE_KEY_VALUE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        parent::getAttributeValueFormatSet($strKey, $value)
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatGet($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case BaseConstPermission::ATTRIBUTE_KEY_VALUE:
                $result = $value;
                break;

            default:
                $result = parent::getAttributeValueSaveFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatSet($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case BaseConstPermission::ATTRIBUTE_KEY_VALUE:
                $result = $value;
                break;

            default:
                $result = parent::getAttributeValueSaveFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



}