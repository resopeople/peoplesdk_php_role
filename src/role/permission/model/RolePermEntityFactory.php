<?php
/**
 * This class allows to define role permission entity factory class.
 * Role permission entity factory is basic role permission factory,
 * allows to provide new role permission entities.
 *
 * Role permission entity factory uses the following specified configuration, to get and hydrate role permission:
 * [
 *     Role permission entity configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\role\role\permission\model;

use liberty_code\role_model\role\permission\model\RolePermEntityFactory as BaseRolePermEntityFactory;

use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\factory\library\ConstEntityFactory;
use liberty_code\role\permission\api\PermissionInterface;
use people_sdk\role\role\permission\model\RolePermEntity;



/**
 * @method RolePermEntity getObjEntity(array $tabValue = array(), array $tabConfig = null) @inheritdoc
 * @method null|RolePermEntity getObjPermission(array $tabConfig = array(), string $strConfigKey = null, PermissionInterface $objPermission = null) @inheritdoc
 */
class RolePermEntityFactory extends BaseRolePermEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => RolePermEntity::class
        );
    }



    /**
     * @inheritdoc
     */
    protected function getObjEntityNew(array $tabConfig = null)
    {
        // Init var
        $objValidator = $this->getObjInstance(ValidatorInterface::class);
        $result = new RolePermEntity(
            array(),
            $objValidator
        );

        // Return result
        return $result;
    }



}