<?php
/**
 * This class allows to define role permission entity collection class.
 * Role permission entity collection is basic role permission entity collection,
 * used to store role permission entities.
 * key => RolePermEntity.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\role\role\permission\model;

use liberty_code\role_model\role\permission\model\RolePermEntityCollection as BaseRolePermEntityCollection;

use people_sdk\role\role\permission\model\RolePermEntity;



/**
 * @method null|RolePermEntity getItem(string $strKey) @inheritdoc
 * @method null|RolePermEntity getObjPermission(string $strKey) @inheritdoc
 * @method string setItem(RolePermEntity $objEntity) @inheritdoc
 * @method string setPermission(RolePermEntity $objPermission) @inheritdoc
 * @method RolePermEntity removePermission(string $strKey) @inheritdoc
 */
class RolePermEntityCollection extends BaseRolePermEntityCollection
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixItemClassPath()
    {
        // Return result
        return RolePermEntity::class;
    }



}