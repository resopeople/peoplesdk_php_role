<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\role\role\permission\library;



class ConstRolePerm
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Attribute configuration
    const ATTRIBUTE_NAME_SAVE_KEY = 'key';
    const ATTRIBUTE_NAME_SAVE_VALUE = 'value';



}