<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\role\role\requisition\request\info\library;



class ConstRoleSndInfo
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Headers configuration
    const HEADER_KEY_CURRENT_INCLUDE = 'Current-Include';
    const HEADER_KEY_PERM_FULL_UPDATE = 'Permission-Full-Update';



    // URL arguments configuration
    const URL_ARG_KEY_CURRENT_INCLUDE = 'current-include';
    const URL_ARG_KEY_PERM_FULL_UPDATE = 'permission-full-update';



}