<?php
/**
 * This class allows to define role entity collection class.
 * Role entity collection is basic role entity collection,
 * used to store role entities.
 * key => RolePermEntity.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\role\role\model;

use liberty_code\role_model\role\model\RoleEntityCollection as BaseRoleEntityCollection;

use people_sdk\role\role\model\RoleEntity;



/**
 * @method null|RoleEntity getItem(string $strKey) @inheritdoc
 * @method null|RoleEntity getObjRole(string $strName) @inheritdoc
 * @method string setItem(RoleEntity $objEntity) @inheritdoc
 * @method string setRole(RoleEntity $objRole) @inheritdoc
 * @method RoleEntity removeRole(string $strName) @inheritdoc
 */
class RoleEntityCollection extends BaseRoleEntityCollection
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixItemClassPath()
    {
        // Return result
        return RoleEntity::class;
    }



}