<?php
/**
 * This class allows to define role entity factory class.
 * Role entity factory allows to provide new role entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\role\role\model;

use people_sdk\library\model\entity\factory\model\DefaultEntityFactory;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\api\EntityCollectionInterface;
use liberty_code\model\entity\factory\library\ConstEntityFactory as BaseConstEntityFactory;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use liberty_code\role_model\role\library\ConstRole as BaseConstRole;
use people_sdk\library\model\entity\factory\library\ConstEntityFactory;
use people_sdk\role\role\permission\model\RolePermEntityFactory;
use people_sdk\role\role\library\ConstRole;
use people_sdk\role\role\exception\RolePermEntityFactoryInvalidFormatException;
use people_sdk\role\role\exception\RolePermEntityFactoryExecConfigInvalidFormatException;
use people_sdk\role\role\model\RoleEntity;



/**
 *
 * @method null|RolePermEntityFactory getObjRolePermEntityFactory() Get role permission entity factory object.
 * @method null|array getTabRolePermEntityFactoryExecConfig() Get role permission entity factory execution configuration array.
 * @method RoleEntity getObjEntity(array $tabValue = array(), array $tabConfig = null) @inheritdoc
 * @method void setObjRolePermEntityFactory(null|RolePermEntityFactory $objRolePermEntityFactory) Set role permission entity factory object.
 * @method void setTabRolePermEntityFactoryExecConfig(null|array $tabConfig) Set role permission entity factory execution configuration array.
 */
class RoleEntityFactory extends DefaultEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|RolePermEntityFactory $objRolePermEntityFactory
     * @param null|array $tabRolePermEntityFactoryExecConfig = null
     */
    public function __construct(
        ProviderInterface $objProvider,
        EntityCollectionInterface $objEntityCollection = null,
        RolePermEntityFactory $objRolePermEntityFactory = null,
        array $tabRolePermEntityFactoryExecConfig = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objProvider,
            $objEntityCollection
        );

        // Init role permission entity factory
        $this->setObjRolePermEntityFactory($objRolePermEntityFactory);

        // Init role permission entity factory execution config
        $this->setTabRolePermEntityFactoryExecConfig($tabRolePermEntityFactoryExecConfig);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstRole::DATA_KEY_ROLE_PERM_ENTITY_FACTORY))
        {
            $this->__beanTabData[ConstRole::DATA_KEY_ROLE_PERM_ENTITY_FACTORY] = null;
        }

        if(!$this->beanExists(ConstRole::DATA_KEY_ROLE_PERM_ENTITY_FACTORY_EXEC_CONFIG))
        {
            $this->__beanTabData[ConstRole::DATA_KEY_ROLE_PERM_ENTITY_FACTORY_EXEC_CONFIG] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstRole::DATA_KEY_ROLE_PERM_ENTITY_FACTORY,
            ConstRole::DATA_KEY_ROLE_PERM_ENTITY_FACTORY_EXEC_CONFIG
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstRole::DATA_KEY_ROLE_PERM_ENTITY_FACTORY:
                    RolePermEntityFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstRole::DATA_KEY_ROLE_PERM_ENTITY_FACTORY_EXEC_CONFIG:
                    RolePermEntityFactoryExecConfigInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            BaseConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => RoleEntity::class,
            ConstEntityFactory::TAB_CONFIG_KEY_SELECT_ENTITY_ATTRIBUTE_KEY_ID => BaseConstRole::ATTRIBUTE_KEY_ID
        );
    }



    /**
     * @inheritdoc
     */
    protected function getObjEntityNewEngine(array $tabConfig = null)
    {
        // Init var
        $objValidator = $this->getObjInstance(ValidatorInterface::class);
        $objDateTimeFactory = $this->getObjInstance(DateTimeFactoryInterface::class);
        $result = new RoleEntity(
            array(),
            $objValidator,
            $objDateTimeFactory,
            $this->getObjRolePermEntityFactory(),
            $this->getTabRolePermEntityFactoryExecConfig()
        );

        // Return result
        return $result;
    }



}