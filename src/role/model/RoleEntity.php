<?php
/**
 * This class allows to define role entity class.
 * Role entity is basic role entity,
 * allows to design role.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\role\role\model;

use liberty_code\role_model\role\model\RoleEntity as BaseRoleEntity;

use DateTime;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\repository\library\ConstSaveEntity;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use liberty_code\role_model\role\library\ConstRole as BaseConstRole;
use people_sdk\library\model\entity\library\ToolBoxEntity;
use people_sdk\library\model\entity\requisition\response\library\ToolBoxResponseEntityCollection;
use people_sdk\role\role\permission\library\ConstRolePerm;
use people_sdk\role\role\permission\model\RolePermEntityCollection;
use people_sdk\role\role\permission\model\RolePermEntityFactory;
use people_sdk\role\role\library\ConstRole;



/**
 * @property null|integer $intAttrId
 * @property null|string|DateTime $attrDtCreate
 * @property null|string|DateTime $attrDtUpdate
 * @property null|string $strAttrName
 * @property null|RolePermEntityCollection $objAttrRolePermEntityCollection
 */
class RoleEntity extends BaseRoleEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Role permission entity factory instance.
     * @var null|RolePermEntityFactory
     */
    protected $objRolePermEntityFactory;



    /**
     * DI: Role permission entity factory execution configuration.
     * @var null|array
     */
    protected $tabRolePermEntityFactoryExecConfig;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|RolePermEntityFactory $objRolePermEntityFactory = null
     * @param null|array $tabRolePermEntityFactoryExecConfig = null
     */
    public function __construct(
        array $tabValue = array(),
        ValidatorInterface $objValidator = null,
        DateTimeFactoryInterface $objDateTimeFactory = null,
        RolePermEntityFactory $objRolePermEntityFactory = null,
        array $tabRolePermEntityFactoryExecConfig = null
    )
    {
        // Init properties
        $this->objRolePermEntityFactory = $objRolePermEntityFactory;
        $this->tabRolePermEntityFactoryExecConfig = $tabRolePermEntityFactoryExecConfig;

        // Call parent constructor
        parent::__construct($tabValue, $objValidator, $objDateTimeFactory);
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Init var
        $tabUpdateConfig = array(
            // Select and update attribute id
            BaseConstRole::ATTRIBUTE_KEY_ID => [
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstRole::ATTRIBUTE_NAME_SAVE_ID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Select and update attribute datetime create
            BaseConstRole::ATTRIBUTE_KEY_DT_CREATE => [
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstRole::ATTRIBUTE_NAME_SAVE_DT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Select and update attribute datetime update
            BaseConstRole::ATTRIBUTE_KEY_DT_UPDATE => [
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstRole::ATTRIBUTE_NAME_SAVE_DT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Select and update attribute name
            BaseConstRole::ATTRIBUTE_KEY_NAME => [
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstRole::ATTRIBUTE_NAME_SAVE_NAME,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Select and update attribute permission entity collection
            BaseConstRole::ATTRIBUTE_KEY_ROLE_PERM_ENTITY_COLLECTION => [
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstRole::ATTRIBUTE_NAME_SAVE_ROLE_PERM_ENTITY_COLLECTION,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => new RolePermEntityCollection()
            ]
        );
        // Select attribute config
        $result = array_filter(
            parent::getTabConfig(),
            function(array $tabAttrConfig) use ($tabUpdateConfig) {
                $strAttrKey = $tabAttrConfig[ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY];

                return array_key_exists(
                    $strAttrKey,
                    $tabUpdateConfig
                );
            }
        );
        // Format attribute config
        $result = array_map(
            function(array $tabAttrConfig) use ($tabUpdateConfig) {
                $strAttrKey = $tabAttrConfig[ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY];
                $tabUpdateAttrConfig = $tabUpdateConfig[$strAttrKey];

                if(array_key_exists(ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE, $tabAttrConfig))
                {
                    unset($tabAttrConfig[ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE]);
                }

                return array_merge(
                    $tabAttrConfig,
                    $tabUpdateAttrConfig
                );
            },
            $result
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init var
        $tabRuleConfig = parent::getTabRuleConfig();
        $result = array_merge(
            $tabRuleConfig,
            array(
                BaseConstRole::ATTRIBUTE_KEY_ID => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => [
                                    'is_null',
                                    [
                                        'callable',
                                        [
                                            'valid_callable' => function() {return $this->checkIsNew();}
                                        ]
                                    ]
                                ],
                                'is-valid-id' => [
                                    [
                                        'type_numeric',
                                        ['integer_only_require' => true]
                                    ],
                                    [
                                        'compare_greater',
                                        [
                                            'compare_value' => 0,
                                            'equal_enable_require' => false
                                        ]
                                    ],
                                    [
                                        'callable',
                                        [
                                            'valid_callable' => function() {return (!$this->checkIsNew());}
                                        ]
                                    ]
                                ]
                            ],
                            'error_message_pattern' => '%1$s must be null if new, or a valid ID else.'
                        ]
                    ]
                ],
                BaseConstRole::ATTRIBUTE_KEY_DT_CREATE => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => ['is_null'],
                                'is-valid-date' => $tabRuleConfig[BaseConstRole::ATTRIBUTE_KEY_DT_CREATE]
                            ],
                            'error_message_pattern' => '%1$s must be null or a date.'
                        ]
                    ]
                ],
                BaseConstRole::ATTRIBUTE_KEY_DT_UPDATE => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => ['is_null'],
                                'is-valid-date' => $tabRuleConfig[BaseConstRole::ATTRIBUTE_KEY_DT_UPDATE]
                            ],
                            'error_message_pattern' => '%1$s must be null or a date.'
                        ]
                    ]
                ],
                BaseConstRole::ATTRIBUTE_KEY_NAME => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => ['is_null'],
                                'is-valid-string' => $tabRuleConfig[BaseConstRole::ATTRIBUTE_KEY_NAME]
                            ],
                            'error_message_pattern' => '%1$s must be null or a string not empty'
                        ]
                    ]
                ],
                BaseConstRole::ATTRIBUTE_KEY_ROLE_PERM_ENTITY_COLLECTION => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => ['is_null'],
                                'is-valid-collection' => [
                                    [
                                        'type_object',
                                        [
                                            'class_path' => [RolePermEntityCollection::class]
                                        ]
                                    ],
                                    ['validation_entity_collection']
                                ]
                            ],
                            'error_message_pattern' => '%1$s must be null or a valid role permission collection.'
                        ]
                    ]
                ]
            )
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case BaseConstRole::ATTRIBUTE_KEY_ID:
            case BaseConstRole::ATTRIBUTE_KEY_DT_CREATE:
            case BaseConstRole::ATTRIBUTE_KEY_DT_UPDATE:
            case BaseConstRole::ATTRIBUTE_KEY_NAME:
            case BaseConstRole::ATTRIBUTE_KEY_ROLE_PERM_ENTITY_COLLECTION:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        parent::getAttributeValueFormatSet($strKey, $value)
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatGet($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case BaseConstRole::ATTRIBUTE_KEY_ID:
                $result = $value;
                break;

            case BaseConstRole::ATTRIBUTE_KEY_ROLE_PERM_ENTITY_COLLECTION:
                $result = (
                    ($value instanceof RolePermEntityCollection) ?
                        ToolBoxEntity::getTabEntityCollectionDataSave($value) :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueSaveFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatSet($strKey, $value)
    {
        // Init var
        $objRolePermEntityFactory = $this->objRolePermEntityFactory;

        // Format by attribute
        switch($strKey)
        {
            case BaseConstRole::ATTRIBUTE_KEY_ID:
                $result = $value;
                break;

            case BaseConstRole::ATTRIBUTE_KEY_ROLE_PERM_ENTITY_COLLECTION:
                $result = $value;
                if((!is_null($objRolePermEntityFactory)) && is_array($value))
                {
                    // Get index array of data
                    $tabData = array_map(
                        function($data) {
                            $result = $data;

                            if(is_array($data))
                            {
                                $tabIncludeKey = array(
                                    ConstRolePerm::ATTRIBUTE_NAME_SAVE_KEY,
                                    ConstRolePerm::ATTRIBUTE_NAME_SAVE_VALUE
                                );
                                $result = array_filter(
                                    $data,
                                    function($key) use ($tabIncludeKey) {
                                        return (
                                            (!is_string($key)) ||
                                            in_array($key, $tabIncludeKey)
                                        );
                                    },
                                    ARRAY_FILTER_USE_KEY
                                );
                            }

                            return $result;
                        },
                        ToolBoxResponseEntityCollection::getTabEntityDataFromData($value)
                    );

                    // Get role permission entity collection, from index array of data
                    $result = new RolePermEntityCollection();
                    ToolBoxEntity::hydrateSaveEntityCollection(
                        $result,
                        $objRolePermEntityFactory,
                        $tabData,
                        true,
                        false,
                        $this->tabRolePermEntityFactoryExecConfig
                    );
                }
                break;

            default:
                $result = parent::getAttributeValueSaveFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



}