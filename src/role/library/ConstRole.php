<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\role\role\library;



class ConstRole
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_ROLE_PERM_ENTITY_FACTORY = 'objRolePermEntityFactory';
    const DATA_KEY_ROLE_PERM_ENTITY_FACTORY_EXEC_CONFIG = 'tabRolePermEntityFactoryExecConfig';



    // Attribute configuration
    const ATTRIBUTE_NAME_SAVE_ID = 'id';
    const ATTRIBUTE_NAME_SAVE_DT_CREATE = 'dt-create';
    const ATTRIBUTE_NAME_SAVE_DT_UPDATE = 'dt-update';
    const ATTRIBUTE_NAME_SAVE_NAME = 'name';
    const ATTRIBUTE_NAME_SAVE_ROLE_PERM_ENTITY_COLLECTION = 'permission';



    // Sub-action type configuration
    const SUB_ACTION_TYPE_ADMIN = 'admin';



    // Exception message constants
    const EXCEPT_MSG_ROLE_PERM_ENTITY_FACTORY_INVALID_FORMAT =
        'Following role permission entity factory "%1$s" invalid! It must be a role permission entity factory object.';
    const EXCEPT_MSG_ROLE_PERM_ENTITY_FACTORY_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the role permission entity factory execution configuration standard.';



}