<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\role\profile\browser\library;



class ConstRoleProfileBrowser
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Attribute configuration
    const ATTRIBUTE_KEY_CRIT_ENABLE_LIKE_PERM_KEY = 'strAttrCritEnableLikePermKey';
    const ATTRIBUTE_KEY_CRIT_ENABLE_EQUAL_PERM_KEY = 'strAttrCritEnableEqualPermKey';
    const ATTRIBUTE_KEY_CRIT_ENABLE_IN_PERM_KEY = 'tabAttrCritEnableInPermKey';
    const ATTRIBUTE_KEY_CRIT_DISABLE_LIKE_PERM_KEY = 'strAttrCritDisableLikePermKey';
    const ATTRIBUTE_KEY_CRIT_DISABLE_EQUAL_PERM_KEY = 'strAttrCritDisableEqualPermKey';
    const ATTRIBUTE_KEY_CRIT_DISABLE_IN_PERM_KEY = 'tabAttrCritDisableInPermKey';
    const ATTRIBUTE_KEY_CRIT_HAS_EQUAL_ROLE_ID = 'intAttrCritHasEqualRoleId';
    const ATTRIBUTE_KEY_CRIT_HAS_IN_ROLE_ID = 'tabAttrCritHasInRoleId';
    const ATTRIBUTE_KEY_CRIT_NOT_HAS_EQUAL_ROLE_ID = 'intAttrCritNotHasEqualRoleId';
    const ATTRIBUTE_KEY_CRIT_NOT_HAS_IN_ROLE_ID = 'tabAttrCritNotHasInRoleId';
    const ATTRIBUTE_KEY_CRIT_HAS_LIKE_ROLE_NAME = 'strAttrCritHasLikeRoleName';
    const ATTRIBUTE_KEY_CRIT_HAS_EQUAL_ROLE_NAME = 'strAttrCritHasEqualRoleName';
    const ATTRIBUTE_KEY_CRIT_HAS_IN_ROLE_NAME = 'tabAttrCritHasInRoleName';
    const ATTRIBUTE_KEY_CRIT_NOT_HAS_LIKE_ROLE_NAME = 'strAttrCritNotHasLikeRoleName';
    const ATTRIBUTE_KEY_CRIT_NOT_HAS_EQUAL_ROLE_NAME = 'strAttrCritNotHasEqualRoleName';
    const ATTRIBUTE_KEY_CRIT_NOT_HAS_IN_ROLE_NAME = 'tabAttrCritNotHasInRoleName';

    const ATTRIBUTE_ALIAS_CRIT_ENABLE_LIKE_PERM_KEY = 'crit-enable-like-permission-key';
    const ATTRIBUTE_ALIAS_CRIT_ENABLE_EQUAL_PERM_KEY = 'crit-enable-equal-permission-key';
    const ATTRIBUTE_ALIAS_CRIT_ENABLE_IN_PERM_KEY = 'crit-enable-in-permission-key';
    const ATTRIBUTE_ALIAS_CRIT_DISABLE_LIKE_PERM_KEY = 'crit-disable-like-permission-key';
    const ATTRIBUTE_ALIAS_CRIT_DISABLE_EQUAL_PERM_KEY = 'crit-disable-equal-permission-key';
    const ATTRIBUTE_ALIAS_CRIT_DISABLE_IN_PERM_KEY = 'crit-disable-in-permission-key';
    const ATTRIBUTE_ALIAS_CRIT_HAS_EQUAL_ROLE_ID = 'crit-has-equal-role-id';
    const ATTRIBUTE_ALIAS_CRIT_HAS_IN_ROLE_ID = 'crit-has-in-role-id';
    const ATTRIBUTE_ALIAS_CRIT_NOT_HAS_EQUAL_ROLE_ID = 'crit-not-has-equal-role-id';
    const ATTRIBUTE_ALIAS_CRIT_NOT_HAS_IN_ROLE_ID = 'crit-not-has-in-role-id';
    const ATTRIBUTE_ALIAS_CRIT_HAS_LIKE_ROLE_NAME = 'crit-has-like-role-name';
    const ATTRIBUTE_ALIAS_CRIT_HAS_EQUAL_ROLE_NAME = 'crit-has-equal-role-name';
    const ATTRIBUTE_ALIAS_CRIT_HAS_IN_ROLE_NAME = 'crit-has-in-role-name';
    const ATTRIBUTE_ALIAS_CRIT_NOT_HAS_LIKE_ROLE_NAME = 'crit-not-has-like-role-name';
    const ATTRIBUTE_ALIAS_CRIT_NOT_HAS_EQUAL_ROLE_NAME = 'crit-not-has-equal-role-name';
    const ATTRIBUTE_ALIAS_CRIT_NOT_HAS_IN_ROLE_NAME = 'crit-not-has-in-role-name';



}