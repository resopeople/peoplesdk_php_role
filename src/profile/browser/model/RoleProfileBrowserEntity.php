<?php
/**
 * This class allows to define role profile browser entity class.
 * Role profile browser entity allows to define attributes,
 * to search role profile entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\role\profile\browser\model;

use people_sdk\library\browser\model\DefaultBrowserEntity;

use liberty_code\model\entity\library\ConstEntity;
use people_sdk\role\profile\browser\library\ConstRoleProfileBrowser;



/**
 * @property null|string $strAttrCritEnableLikePermKey
 * @property null|string $strAttrCritEnableEqualPermKey
 * @property null|string[] $tabAttrCritEnableInPermKey
 * @property null|string $strAttrCritDisableLikePermKey
 * @property null|string $strAttrCritDisableEqualPermKey
 * @property null|string[] $tabAttrCritDisableInPermKey
 * @property null|integer $intAttrCritHasEqualRoleId
 * @property null|integer[] $tabAttrCritHasInRoleId
 * @property null|integer $intAttrCritNotHasEqualRoleId
 * @property null|integer[] $tabAttrCritNotHasInRoleId
 * @property null|string $strAttrCritHasLikeRoleName
 * @property null|string $strAttrCritHasEqualRoleName
 * @property null|string[] $tabAttrCritHasInRoleName
 * @property null|string $strAttrCritNotHasLikeRoleName
 * @property null|string $strAttrCritNotHasEqualRoleName
 * @property null|string[] $tabAttrCritNotHasInRoleName
 */
class RoleProfileBrowserEntity extends DefaultBrowserEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods entity overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return array_merge(
            parent::getTabConfig(),
            array(
                // Attribute criteria enable like permission key
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_ENABLE_LIKE_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRoleProfileBrowser::ATTRIBUTE_ALIAS_CRIT_ENABLE_LIKE_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria enable equal permission key
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_ENABLE_EQUAL_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRoleProfileBrowser::ATTRIBUTE_ALIAS_CRIT_ENABLE_EQUAL_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria enable in permission key
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_ENABLE_IN_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRoleProfileBrowser::ATTRIBUTE_ALIAS_CRIT_ENABLE_IN_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria disable like permission key
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_DISABLE_LIKE_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRoleProfileBrowser::ATTRIBUTE_ALIAS_CRIT_DISABLE_LIKE_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria disable equal permission key
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_DISABLE_EQUAL_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRoleProfileBrowser::ATTRIBUTE_ALIAS_CRIT_DISABLE_EQUAL_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria disable in permission key
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_DISABLE_IN_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRoleProfileBrowser::ATTRIBUTE_ALIAS_CRIT_DISABLE_IN_PERM_KEY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria has equal role id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_HAS_EQUAL_ROLE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRoleProfileBrowser::ATTRIBUTE_ALIAS_CRIT_HAS_EQUAL_ROLE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria has in role id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_HAS_IN_ROLE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRoleProfileBrowser::ATTRIBUTE_ALIAS_CRIT_HAS_IN_ROLE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not has equal role id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_NOT_HAS_EQUAL_ROLE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRoleProfileBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_HAS_EQUAL_ROLE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not has in role id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_NOT_HAS_IN_ROLE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRoleProfileBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_HAS_IN_ROLE_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria has like role name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_HAS_LIKE_ROLE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRoleProfileBrowser::ATTRIBUTE_ALIAS_CRIT_HAS_LIKE_ROLE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria has equal role name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_HAS_EQUAL_ROLE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRoleProfileBrowser::ATTRIBUTE_ALIAS_CRIT_HAS_EQUAL_ROLE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria has in role name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_HAS_IN_ROLE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRoleProfileBrowser::ATTRIBUTE_ALIAS_CRIT_HAS_IN_ROLE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not has like role name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_NOT_HAS_LIKE_ROLE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRoleProfileBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_HAS_LIKE_ROLE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not has equal role name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_NOT_HAS_EQUAL_ROLE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRoleProfileBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_HAS_EQUAL_ROLE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute criteria not has in role name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_NOT_HAS_IN_ROLE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstRoleProfileBrowser::ATTRIBUTE_ALIAS_CRIT_NOT_HAS_IN_ROLE_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ]
            )
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init rule configuration
        $tabRuleConfigValidId = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-integer' => [
                            [
                                'type_numeric',
                                ['integer_only_require' => true]
                            ],
                            [
                                'compare_greater',
                                [
                                    'compare_value' => 0,
                                    'equal_enable_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a strict positive integer.'
                ]
            ]
        );
        $tabRuleConfigValidTabId = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-iterate-integer' => [
                            [
                                'type_array',
                                [
                                    'index_only_require' => true
                                ]
                            ],
                            [
                                'sub_rule_iterate',
                                [
                                    'rule_config' => [
                                        [
                                            'type_numeric',
                                            ['integer_only_require' => true]
                                        ],
                                        [
                                            'compare_greater',
                                            [
                                                'compare_value' => 0,
                                                'equal_enable_require' => false
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or an array of strict positive integers.'
                ]
            ]
        );
        $tabRuleConfigValidString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-string' => [
                            'type_string',
                            [
                                'sub_rule_not',
                                [
                                    'rule_config' => ['is_empty']
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a string not empty.'
                ]
            ]
        );
        $tabRuleConfigValidTabString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-iterate-string' => [
                            [
                                'type_array',
                                [
                                    'index_only_require' => true
                                ]
                            ],
                            [
                                'sub_rule_iterate',
                                [
                                    'rule_config' => [
                                        'type_string',
                                        [
                                            'sub_rule_not',
                                            [
                                                'rule_config' => ['is_empty']
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or an array of strings not empty.'
                ]
            ]
        );

        // Init var
        $result = array_merge(
            parent::getTabRuleConfig(),
            array(
                ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_ENABLE_LIKE_PERM_KEY => $tabRuleConfigValidString,
                ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_ENABLE_EQUAL_PERM_KEY => $tabRuleConfigValidString,
                ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_ENABLE_IN_PERM_KEY => $tabRuleConfigValidTabString,
                ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_DISABLE_LIKE_PERM_KEY => $tabRuleConfigValidString,
                ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_DISABLE_EQUAL_PERM_KEY => $tabRuleConfigValidString,
                ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_DISABLE_IN_PERM_KEY => $tabRuleConfigValidTabString,
                ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_HAS_EQUAL_ROLE_ID => $tabRuleConfigValidId,
                ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_HAS_IN_ROLE_ID => $tabRuleConfigValidTabId,
                ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_NOT_HAS_EQUAL_ROLE_ID => $tabRuleConfigValidId,
                ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_NOT_HAS_IN_ROLE_ID => $tabRuleConfigValidTabId,
                ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_HAS_LIKE_ROLE_NAME => $tabRuleConfigValidString,
                ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_HAS_EQUAL_ROLE_NAME => $tabRuleConfigValidString,
                ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_HAS_IN_ROLE_NAME => $tabRuleConfigValidTabString,
                ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_NOT_HAS_LIKE_ROLE_NAME => $tabRuleConfigValidString,
                ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_NOT_HAS_EQUAL_ROLE_NAME => $tabRuleConfigValidString,
                ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_NOT_HAS_IN_ROLE_NAME => $tabRuleConfigValidTabString
            )
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_ENABLE_LIKE_PERM_KEY:
            case ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_ENABLE_EQUAL_PERM_KEY:
            case ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_DISABLE_LIKE_PERM_KEY:
            case ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_DISABLE_EQUAL_PERM_KEY:
            case ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_HAS_LIKE_ROLE_NAME:
            case ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_HAS_EQUAL_ROLE_NAME:
            case ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_NOT_HAS_LIKE_ROLE_NAME:
            case ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_NOT_HAS_EQUAL_ROLE_NAME:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        $value
                );
                break;

            case ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_ENABLE_IN_PERM_KEY:
            case ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_DISABLE_IN_PERM_KEY:
            case ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_HAS_IN_ROLE_NAME:
            case ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_NOT_HAS_IN_ROLE_NAME:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        ((is_array($value)) ? array_values($value) : $value)
                );
                break;

            case ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_HAS_EQUAL_ROLE_ID:
            case ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_NOT_HAS_EQUAL_ROLE_ID:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_string($value) && ctype_digit($value)) ?
                                intval($value) :
                                $value
                        )
                );
                break;

            case ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_HAS_IN_ROLE_ID:
            case ConstRoleProfileBrowser::ATTRIBUTE_KEY_CRIT_NOT_HAS_IN_ROLE_ID:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_array($value)) ?
                                array_values(array_map(
                                    function($value){
                                        return (
                                            (is_string($value) && ctype_digit($value)) ?
                                                intval($value) :
                                                $value
                                        );
                                    },
                                    $value
                                )) :
                                $value
                        )
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



}