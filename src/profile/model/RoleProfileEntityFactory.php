<?php
/**
 * This class allows to define role profile factory class.
 * Role profile factory allows to provide new role profile entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\role\profile\model;

use people_sdk\library\model\entity\factory\model\DefaultEntityFactory;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\model\entity\api\EntityCollectionInterface;
use people_sdk\role\profile\library\ConstRoleProfile;
use people_sdk\role\profile\exception\SubjPermEntityFactoryExecConfigInvalidFormatException;
use people_sdk\role\profile\exception\RoleEntityFactoryExecConfigInvalidFormatException;



/**
 * @method null|array getTabSubjPermEntityFactoryExecConfig() Get subject permission entity factory execution configuration array.
 * @method null|array getTabRoleEntityFactoryExecConfig() Get role entity factory execution configuration array.
 * @method void setTabSubjPermEntityFactoryExecConfig(null|array $tabSubjPermEntityFactoryExecConfig) Set subject permission entity factory execution configuration array.
 * @method void setTabRoleEntityFactoryExecConfig(null|array $tabRoleEntityFactoryExecConfig) Set role entity factory execution configuration array.
 */
abstract class RoleProfileEntityFactory extends DefaultEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|array $tabSubjPermEntityFactoryExecConfig = null
     * @param null|array $tabRoleEntityFactoryExecConfig = null
     */
    public function __construct(
        ProviderInterface $objProvider,
        EntityCollectionInterface $objEntityCollection = null,
        array $tabSubjPermEntityFactoryExecConfig = null,
        array $tabRoleEntityFactoryExecConfig = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objProvider,
            $objEntityCollection
        );

        // Init subject permission entity factory execution config
        $this->setTabSubjPermEntityFactoryExecConfig($tabSubjPermEntityFactoryExecConfig);

        // Init role entity factory execution config
        $this->setTabRoleEntityFactoryExecConfig($tabRoleEntityFactoryExecConfig);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstRoleProfile::DATA_KEY_SUBJ_PERM_ENTITY_FACTORY_EXEC_CONFIG))
        {
            $this->__beanTabData[ConstRoleProfile::DATA_KEY_SUBJ_PERM_ENTITY_FACTORY_EXEC_CONFIG] = null;
        }

        if(!$this->beanExists(ConstRoleProfile::DATA_KEY_ROLE_ENTITY_FACTORY_EXEC_CONFIG))
        {
            $this->__beanTabData[ConstRoleProfile::DATA_KEY_ROLE_ENTITY_FACTORY_EXEC_CONFIG] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstRoleProfile::DATA_KEY_SUBJ_PERM_ENTITY_FACTORY_EXEC_CONFIG,
            ConstRoleProfile::DATA_KEY_ROLE_ENTITY_FACTORY_EXEC_CONFIG
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstRoleProfile::DATA_KEY_SUBJ_PERM_ENTITY_FACTORY_EXEC_CONFIG:
                    SubjPermEntityFactoryExecConfigInvalidFormatException::setCheck($value);
                    break;

                case ConstRoleProfile::DATA_KEY_ROLE_ENTITY_FACTORY_EXEC_CONFIG:
                    RoleEntityFactoryExecConfigInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



}