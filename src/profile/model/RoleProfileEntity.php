<?php
/**
 * This class allows to define role profile entity class.
 * Role profile entity allows to design a specific profile entity,
 * containing subject permission entities, to save related permissions, and can be used as permission subject,
 * and role entities, to save related roles, and can be used as role subject,
 * for authorization.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\role\profile\model;

use liberty_code\model\entity\repository\model\SaveConfigEntity;
use liberty_code\role\permission\subject\api\PermissionSubjectInterface;
use liberty_code\role\role\subject\api\RoleSubjectInterface;

use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\exception\ValidationFailException;
use liberty_code\model\entity\repository\library\ConstSaveEntity;
use liberty_code\role\permission\api\PermissionCollectionInterface;
use liberty_code\role\role\api\RoleCollectionInterface;
use people_sdk\library\model\entity\library\ToolBoxEntity;
use people_sdk\library\model\entity\factory\library\ConstEntityFactory;
use people_sdk\library\model\entity\requisition\response\library\ToolBoxResponseEntityCollection;
use people_sdk\role\permission\subject\library\ConstSubjPerm;
use people_sdk\role\permission\subject\model\SubjPermEntityCollection;
use people_sdk\role\permission\subject\model\SubjPermEntityFactory;
use people_sdk\role\role\library\ConstRole;
use people_sdk\role\role\model\RoleEntity;
use people_sdk\role\role\model\RoleEntityCollection;
use people_sdk\role\role\model\RoleEntityFactory;
use people_sdk\role\profile\library\ConstRoleProfile;



/**
 * @property null|SubjPermEntityCollection $objAttrSubjPermEntityCollection
 * @property null|RoleEntityCollection $objAttrRoleEntityCollection
 */
abstract class RoleProfileEntity extends SaveConfigEntity implements PermissionSubjectInterface, RoleSubjectInterface
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Subject permission entity factory instance.
     * @var null|SubjPermEntityFactory
     */
    protected $objSubjPermEntityFactory;



    /**
     * DI: Role entity factory instance.
     * @var null|RoleEntityFactory
     */
    protected $objRoleEntityFactory;



    /**
     * DI: Subject permission entity factory execution configuration.
     * @var null|array
     */
    protected $tabSubjPermEntityFactoryExecConfig;



    /**
     * DI: Role entity factory execution configuration.
     * @var null|array
     */
    protected $tabRoleEntityFactoryExecConfig;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|SubjPermEntityFactory $objSubjPermEntityFactory = null
     * @param null|RoleEntityFactory $objRoleEntityFactory = null
     * @param null|array $tabSubjPermEntityFactoryExecConfig = null
     * @param null|array $tabRoleEntityFactoryExecConfig = null
     */
    public function __construct(
        array $tabValue = array(),
        ValidatorInterface $objValidator = null,
        SubjPermEntityFactory $objSubjPermEntityFactory = null,
        RoleEntityFactory $objRoleEntityFactory = null,
        array $tabSubjPermEntityFactoryExecConfig = null,
        array $tabRoleEntityFactoryExecConfig = null
    )
    {
        // Init properties
        $this->objSubjPermEntityFactory = $objSubjPermEntityFactory;
        $this->objRoleEntityFactory = $objRoleEntityFactory;
        $this->tabSubjPermEntityFactoryExecConfig = $tabSubjPermEntityFactoryExecConfig;
        $this->tabRoleEntityFactoryExecConfig = $tabRoleEntityFactoryExecConfig;

        // Call parent constructor
        parent::__construct($tabValue, $objValidator);
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return array(
            // Attribute subject permission entity collection
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstRoleProfile::ATTRIBUTE_KEY_SUBJ_PERM_ENTITY_COLLECTION,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstRoleProfile::ATTRIBUTE_ALIAS_SUBJ_PERM_ENTITY_COLLECTION,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstRoleProfile::ATTRIBUTE_NAME_SAVE_SUBJ_PERM_ENTITY_COLLECTION,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => new SubjPermEntityCollection()
            ],

            // Attribute role entity collection
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstRoleProfile::ATTRIBUTE_KEY_ROLE_ENTITY_COLLECTION,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstRoleProfile::ATTRIBUTE_ALIAS_ROLE_ENTITY_COLLECTION,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstRoleProfile::ATTRIBUTE_NAME_SAVE_ROLE_ENTITY_COLLECTION,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => new RoleEntityCollection()
            ]
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Return result
        return array(
            ConstRoleProfile::ATTRIBUTE_KEY_SUBJ_PERM_ENTITY_COLLECTION => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => ['is_null'],
                            'is-valid-collection' => [
                                [
                                    'type_object',
                                    [
                                        'class_path' => [SubjPermEntityCollection::class]
                                    ]
                                ],
                                ['validation_entity_collection']
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null or a valid subject permission collection.'
                    ]
                ]
            ],
            ConstRoleProfile::ATTRIBUTE_KEY_ROLE_ENTITY_COLLECTION => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => ['is_null'],
                            'is-valid-collection' => [
                                [
                                    'type_object',
                                    [
                                        'class_path' => [RoleEntityCollection::class]
                                    ]
                                ],
                                ['validation_entity_collection'],
                                [
                                    'new_entity_collection',
                                    [
                                        'new_require' => false
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null or a valid role collection.'
                    ]
                ]
            ]
        );
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {

            case ConstRoleProfile::ATTRIBUTE_KEY_SUBJ_PERM_ENTITY_COLLECTION:
            case ConstRoleProfile::ATTRIBUTE_KEY_ROLE_ENTITY_COLLECTION:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatGet($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case ConstRoleProfile::ATTRIBUTE_KEY_SUBJ_PERM_ENTITY_COLLECTION:
                $result = (
                    ($value instanceof SubjPermEntityCollection) ?
                        ToolBoxEntity::getTabEntityCollectionDataSave($value) :
                        $value
                );
                break;

            case ConstRoleProfile::ATTRIBUTE_KEY_ROLE_ENTITY_COLLECTION:
                $result = (
                    ($value instanceof RoleEntityCollection) ?
                        ToolBoxEntity::getTabEntityCollectionDataSave(
                            $value,
                            function(RoleEntity $objEntity, array $tabData) {
                                return array_filter(
                                    $tabData,
                                    function($strKey) {return ($strKey == ConstRole::ATTRIBUTE_NAME_SAVE_ID);},
                                    ARRAY_FILTER_USE_KEY
                                );
                            }
                        ) :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueSaveFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatSet($strKey, $value)
    {
        // Init var
        $objSubjPermEntityFactory = $this->objSubjPermEntityFactory;
        $objRoleEntityFactory = $this->objRoleEntityFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstRoleProfile::ATTRIBUTE_KEY_SUBJ_PERM_ENTITY_COLLECTION:
                $result = $value;
                if((!is_null($objSubjPermEntityFactory)) && is_array($value))
                {
                    // Get index array of data
                    $tabData = array_map(
                        function($data) {
                            $result = $data;

                            if(is_array($data))
                            {
                                $tabIncludeKey = array(
                                    ConstSubjPerm::ATTRIBUTE_NAME_SAVE_KEY,
                                    ConstSubjPerm::ATTRIBUTE_NAME_SAVE_VALUE
                                );
                                $result = array_filter(
                                    $data,
                                    function($key) use ($tabIncludeKey) {
                                        return (
                                            (!is_string($key)) ||
                                            in_array($key, $tabIncludeKey)
                                        );
                                    },
                                    ARRAY_FILTER_USE_KEY
                                );
                            }

                            return $result;
                        },
                        ToolBoxResponseEntityCollection::getTabEntityDataFromData($value)
                    );

                    // Get subject permission entity collection, from index array of data
                    $result = new SubjPermEntityCollection();
                    ToolBoxEntity::hydrateSaveEntityCollection(
                        $result,
                        $objSubjPermEntityFactory,
                        $tabData,
                        true,
                        false,
                        $this->tabSubjPermEntityFactoryExecConfig
                    );
                }
                break;

            case ConstRoleProfile::ATTRIBUTE_KEY_ROLE_ENTITY_COLLECTION:
                $result = $value;
                if((!is_null($objRoleEntityFactory)) && is_array($value))
                {
                    $result = new RoleEntityCollection();
                    ToolBoxEntity::hydrateSaveEntityCollection(
                        $result,
                        $objRoleEntityFactory,
                        ToolBoxResponseEntityCollection::getTabEntityDataFromData($value),
                        true,
                        false,
                        // Try to select role entity, by id, if required
                        function(array $tabData)
                        {
                            return (
                                (!is_null($this->tabRoleEntityFactoryExecConfig)) ?
                                    (
                                        isset($tabData[ConstRole::ATTRIBUTE_NAME_SAVE_ID]) ?
                                            array_merge(
                                                array(
                                                    ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                        $tabData[ConstRole::ATTRIBUTE_NAME_SAVE_ID]
                                                ),
                                                $this->tabRoleEntityFactoryExecConfig
                                            ) :
                                            $this->tabRoleEntityFactoryExecConfig
                                    ) :
                                    (
                                        isset($tabData[ConstRole::ATTRIBUTE_NAME_SAVE_ID]) ?
                                            array(
                                                ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                    $tabData[ConstRole::ATTRIBUTE_NAME_SAVE_ID]
                                            ) :
                                            null
                                    )
                            );
                        }
                    );
                }
                break;

            default:
                $result = parent::getAttributeValueSaveFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkPermissionEnable($strKey)
    {
        // Return result
        return
            $this->getObjPermissionCollection()->checkPermissionEnable($strKey) ||
            $this->getObjRoleCollection()->checkPermissionEnable($strKey);
    }



    /**
     * @inheritdoc
     */
    public function checkRoleExists($strName)
    {
        // Return result
        return $this->getObjRoleCollection()->checkRoleExists($strName);
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws ValidationFailException
     */
    public function getObjPermissionCollection()
    {
        // Init var
        $result = $this->getAttributeValue(ConstRoleProfile::ATTRIBUTE_KEY_SUBJ_PERM_ENTITY_COLLECTION);

        // Set check attribute valid
        if(!($result instanceof SubjPermEntityCollection))
        {
            $this->setAttributeValid(ConstRoleProfile::ATTRIBUTE_KEY_SUBJ_PERM_ENTITY_COLLECTION);
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws ValidationFailException
     */
    public function getObjRoleCollection()
    {
        // Init var
        $result = $this->getAttributeValue(ConstRoleProfile::ATTRIBUTE_KEY_ROLE_ENTITY_COLLECTION);

        // Set check attribute valid
        if(!($result instanceof RoleEntityCollection))
        {
            $this->setAttributeValid(ConstRoleProfile::ATTRIBUTE_KEY_ROLE_ENTITY_COLLECTION);
        }

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws ValidationFailException
     */
    public function setPermissionCollection(PermissionCollectionInterface $objPermissionCollection)
    {
        // Set check argument value valid
        $objPermissionCollection = $this->getAttributeValueFormatSet(
            ConstRoleProfile::ATTRIBUTE_KEY_SUBJ_PERM_ENTITY_COLLECTION,
            $objPermissionCollection
        );
        $this->setAttributeValueValid(
            ConstRoleProfile::ATTRIBUTE_KEY_SUBJ_PERM_ENTITY_COLLECTION,
            $objPermissionCollection
        );

        // Set value
        $this->setAttributeValue(
            ConstRoleProfile::ATTRIBUTE_KEY_SUBJ_PERM_ENTITY_COLLECTION,
            $objPermissionCollection,
            false
        );
    }



    /**
     * @inheritdoc
     * @throws ValidationFailException
     */
    public function setRoleCollection(RoleCollectionInterface $objRoleCollection)
    {
        // Set check argument value valid
        $objRoleCollection = $this->getAttributeValueFormatSet(
            ConstRoleProfile::ATTRIBUTE_KEY_ROLE_ENTITY_COLLECTION,
            $objRoleCollection
        );
        $this->setAttributeValueValid(
            ConstRoleProfile::ATTRIBUTE_KEY_ROLE_ENTITY_COLLECTION,
            $objRoleCollection
        );

        // Set value
        $this->setAttributeValue(
            ConstRoleProfile::ATTRIBUTE_KEY_ROLE_ENTITY_COLLECTION,
            $objRoleCollection,
            false
        );
    }



}