<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\role\profile\library;



class ConstRoleProfile
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_SUBJ_PERM_ENTITY_FACTORY_EXEC_CONFIG = 'tabSubjPermEntityFactoryExecConfig';
    const DATA_KEY_ROLE_ENTITY_FACTORY_EXEC_CONFIG = 'tabRoleEntityFactoryExecConfig';



    // Attribute configuration
    const ATTRIBUTE_KEY_SUBJ_PERM_ENTITY_COLLECTION = 'objAttrSubjPermEntityCollection';
    const ATTRIBUTE_KEY_ROLE_ENTITY_COLLECTION = 'objAttrRoleEntityCollection';

    const ATTRIBUTE_ALIAS_SUBJ_PERM_ENTITY_COLLECTION = 'permission';
    const ATTRIBUTE_ALIAS_ROLE_ENTITY_COLLECTION = 'role';

    const ATTRIBUTE_NAME_SAVE_SUBJ_PERM_ENTITY_COLLECTION = 'permission';
    const ATTRIBUTE_NAME_SAVE_ROLE_ENTITY_COLLECTION = 'role';



    // Exception message constants
    const EXCEPT_MSG_SUBJ_PERM_ENTITY_FACTORY_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the subject permission entity factory execution configuration standard.';
    const EXCEPT_MSG_ROLE_ENTITY_FACTORY_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the role entity factory execution configuration standard.';



}